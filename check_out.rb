require 'pry'

class CheckOut
  attr_reader :rules, :items

  def initialize(rules)
    @rules = rules
    @items = []
  end

  def scan(item)
    if (existing_item = @items.find { |i| i[:name] == item })
      existing_item[:quantity] += 1
    else
      @items << {name: item, quantity:1}
    end
  end

  def total
    sum_amount = 0

    @items.each do |item|
      sum_amount += rules.apply(item)
    end

    sum_amount
  end
end

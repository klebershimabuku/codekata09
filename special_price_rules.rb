require './rules'
require './item'
require './item_rule'

class SpecialPriceRules < Rules

  def initialize
    do_setup
  end

  def apply(item)
    rule = find_rule(item[:name])

    if rule
      rule.total(item[:quantity])
    else
      find_price(item[:name])
    end
  end

  protected
  def find_price(name)
    @items.find { |x| x.name == name }.price
  end

  def find_rule(name)
    @rules.find { |r| r.name == name }
  end

  def do_setup
    item_a = Item.new('A', 50)
    item_b = Item.new('B', 30)
    item_c = Item.new('C', 20)
    item_d = Item.new('D', 15)

    @items = [item_a, item_b, item_c, item_d]

    item_rule_a = ItemRule.new('A', 3, 130, item_a.price)
    item_rule_b = ItemRule.new('B', 2, 45, item_b.price)

    @rules = [item_rule_a, item_rule_b]
  end

end

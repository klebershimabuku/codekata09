class ItemRule
  attr_accessor :name, :quantity, :special_price, :price

  def initialize(name, quantity, special_price, price)
    @name = name
    @quantity = quantity
    @special_price = special_price
    @price = price
  end

  def total(quantity)
    if (quantity == @quantity)
      @special_price
    elsif (quantity > @quantity)
      operation = quantity.divmod(@quantity)
      quocient = operation[0]
      remainder = operation[1]

      if (remainder == 0)
        @special_price * quocient
      else
        (quocient * @special_price) + (remainder * @price)
      end
    else
      quantity * @price
    end
  end
end

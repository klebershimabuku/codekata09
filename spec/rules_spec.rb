require './check_out'
require './special_price_rules'
require 'pry'

describe Rules do

  let(:rules) { SpecialPriceRules.new }

  def price(goods)
    co = CheckOut.new(rules)
    goods.split(//).each { |item| co.scan(item) }
    co.total
  end

  describe '#totals' do
    it { expect(price("")).to eq(0) }
    it { expect(price("A")).to eq(50) }
    it { expect(price("AB")).to eq(80) }
    it { expect(price("CDBA")).to eq(115) }

    it { expect(price("AA")).to eq(100) }
    it { expect(price("AAA")).to eq(130) }
    it { expect(price("AAAA")).to eq(180) }
    it { expect(price("AAAAA")).to eq(230) }
    it { expect(price("AAAAAA")).to eq(260) }

    it { expect(price("AAAB")).to eq(160) }
    it { expect(price("AAABB")).to eq(175) }
    it { expect(price("AAABBD")).to eq(190) }
    it { expect(price("DABABA")).to eq(190) }
  end

  describe 'incremental' do
    let(:co) { CheckOut.new(SpecialPriceRules.new) }

    it { expect(co.total).to eq(0) }

    it 'increments the total amount' do
      co.scan("A")
      expect(co.total).to eq(50)
      co.scan("B")
      expect(co.total).to eq(80)
      co.scan("A")
      expect(co.total).to eq(130)
      co.scan("A")
      expect(co.total).to eq(160)
      co.scan("B")
      expect(co.total).to eq(175)
    end
  end
end
